#!/usr/bin/perl -w

# xsshfs v0.4
# 
# Copyright David MERCEREAUt <david.mercereau [aro] gmail [.] com>. This program is free software;
# you can redistribute it and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# 
# Please take a look at http://www.gnu.org/licenses/gpl.html
# 
# unsleeper can:
#   v0.4: Aout 2012
# 
# DEPENDANCE:
# - fuse-utils
# - libfuse2
# - sshfs
# - ssh-askpass
# - libgtk2-gladexml-perl
# - perl
#
# Auteur : David MERCEREAUt <david.mercereau [aro] gmail [.] com>
# Contributeur : Laurent CORROCHANO <l.corrochano [aro] free [.] fr>
# Site : http://xsshfs.zici.fr

use lib '/usr/share/xsshfs';
use lib './';

use xsshfs ;

Gtk2->main ; 
