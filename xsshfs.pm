package rappels ;

use strict;
use warnings "all";
use Getopt::Long;
use Config::Tiny;
use Locale::gettext;

# For test translation
bindtextdomain("xsshfs", "./locale");
# For translation
textdomain("xsshfs");

# Version du programme (et des base)
my $version = '0.5'; my $dateVersion = 'Aug 2012';

# parametres generaux du programme
my $params = {
    'version'              => $version,
    'dateversion'          => $dateVersion,
    'programme'            => 'Xsshfs',
    'help'			       => 0,
    'debug'			       => 0,
    'versionOption'	       => 0,
    'configDir'            => "$ENV{HOME}/.config/xsshfs",
    'configParam'          => "$ENV{HOME}/.config/xsshfs/param.ini",
    'configData'           => "$ENV{HOME}/.config/xsshfs/data.ini",
    'tmpFile'              => "/tmp/xsshfs-sortie",
    'comboSetText'         => "",
    'default_user'         => $ENV{USER},
    'default_dir'          => "",
    'default_mountpoint'   => $ENV{HOME},
    'default_port'         => 22,
    'default_keyssh'       => "",
    'default_options'      => "",
    'opt_open_folder'      => 1,
    'opt_file_manager'     => "/usr/bin/xdg-open",
    'opt_path_sshfs'       => "/usr/bin/sshfs",
    'opt_path_fusermount'  => "/bin/fusermount"
};

# Lecture des options ligne de commande
GetOptions(
    'help!'       => \$params->{help},
    'debug!'      => \$params->{debug},
    'version!'    => \$params->{versionOption}
);

if ($params->{versionOption} > 0) {
	print "Programme : $params->{programme}.pl V$params->{version} - ($params->{dateversion})\n";
	exit();
}

if ($params->{help} > 0) {
    print <<TEXTHELP;
Programme : $params->{programme}.pl V$params->{version} - ($params->{dateversion})
    Interface graphique pour SSHFS
Perl version : $]

Usage : $params->{programme}.pl [Option ...]

  Option :
	-debug                      : Active le debug
	-help                       : Affiche cette aide
	-version                    : Affiche la version du programme

DEPENDANCE:
	- sshfs
	- ssh-askpass
	- perl
	- libgtk2-gladexml-perl
	- libimage-librsvg-perl
	- liblocale-gettext-perl
	- libconfig-tiny-perl

LICENSE: 
Copyright David MERCEREAUt <david.mercereau [aro] gmail [.] com>. This program is free software;
you can redistribute it and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

Please take a look at http://www.gnu.org/licenses/gpl.html

Author : David MERCEREAUt <david.mercereau [aro] gmail [.] com>
Contributor : Laurent CORROCHANO <l.corrochano [aro] free [.] fr>
Idea and translation : Fernando CANIZO <conan [aro] lugmen.org [.] ar> http://conan.muriandre.com
Site : http://xsshfs.zici.fr

TEXTHELP
    exit();
}

use vars qw($gladexml) ;
# $gladexml est donc la variable qui fera référence  l'arbre xml
# de l'application.
# On initialise la valeur de la variable quand l'arbre est créé
# dans le module principale

# Préparation des .ini
if (! -d $params->{configDir}) {
	mkdir $params->{configDir}
}
touch($params->{configParam});
touch($params->{configData});
my $configData = Config::Tiny->new;
my $configParam = Config::Tiny->new;
$configData = Config::Tiny->read($params->{configData});
$configParam = Config::Tiny->read($params->{configParam});


############
# Programme principal
############

sub init {	

	# Démarage de l'interface graphique
	$gladexml = $X::gladexml ;
	# init des paramètres
	&param_ini;
	# Actualiser les donner dans la fenêtre
	&actualiser_connexion_sav_combo;
	&actualiser_deconnexion_combo;
}

############
# Fonction déclanché par clique bouton
############

sub on_menu_param_activate {
	debug("Lancement de la fenêtre de configuration");
	$gladexml->get_widget('default_user')->set_text($params->{default_user});
	$gladexml->get_widget('default_dir')->set_text($params->{default_dir});
	$gladexml->get_widget('default_mountpoint')->set_text($params->{default_mountpoint});
	$gladexml->get_widget('default_port')->set_value($params->{default_port});
	$gladexml->get_widget('default_options')->set_text($params->{default_options});
	if ($params->{default_keyssh}) {
		$gladexml->get_widget('default_keyssh')->set_filename($params->{default_keyssh});
	}
	$gladexml->get_widget('opt_open_folder')->set_active($params->{opt_open_folder});
	$gladexml->get_widget('opt_file_manager')->set_text($params->{opt_file_manager});
	$gladexml->get_widget('opt_path_sshfs')->set_text($params->{opt_path_sshfs});
	$gladexml->get_widget('opt_path_fusermount')->set_text($params->{opt_path_fusermount});
	$gladexml->get_widget('Param_Dialog')->run;
}
sub on_about_activate {
	debug("Lancement de la fenêtre d'about");
	$gladexml->get_widget('aboutdialog1')->run;
}
#
sub on_window1_delete_event{
	debug("Fenêtre fermmer : on quitte");
	Gtk2->main_quit ;
}
sub on_bouton_quitter_clicked{
	debug("Bouton quitter cliquer : on quitte");
	Gtk2->main_quit ;
}
sub on_aboutdialog1_close {
	debug("Fermeture de l'about");
	$gladexml->get_widget('aboutdialog1')->hide;
}
sub on_bouton_fermer_erreur_clicked{
	debug("Fermeture de la fenêtre d'erreur");
	$gladexml->get_widget('Erreur_Dialog')->hide;
}
sub fermeture_nom_sav{
	debug("Fermeture de la fenêtre de sauvegarde");
	$gladexml->get_widget('Nom_Sav_Dialog')->hide;
}
sub on_param_cancel_clicked{
	debug("fenêtre de configuration : quit (annulation)");
	$gladexml->get_widget('Param_Dialog')->hide;
}

# Nouveau montage
sub on_bouton_monter_clicked {
	my $sav = shift;
	# On récupérer les données du formulaire
	my $data_user = $gladexml->get_widget('user')->get_text();
	my $data_host = $gladexml->get_widget('host')->get_text();
	my $data_dir = $gladexml->get_widget('dir')->get_text();
	my $data_mountpoint = $gladexml->get_widget('mountpoint')->get_text();
	my $data_port = $gladexml->get_widget('port')->get_text();
	my $data_keyssh = $gladexml->get_widget('keyssh')->get_filenames();
	my $data_options = $gladexml->get_widget('options')->get_text();
	# Vérification si au moin le hosts à été renseigné
	if ($data_host eq "") {
		Erreur_Dialog(gettext("The server address is required"), '')
	} else {
		# Demande d'enregistrement
		if ($sav) {
			$gladexml->get_widget('Sav_Nom')->set_text("$data_user\@$data_host");
			my $Nom_Sav_Dialog = $gladexml->get_widget('Nom_Sav_Dialog')->run;
		}
		mount_sshfs($data_user, $data_host, $data_dir, $data_mountpoint, $data_port, $data_keyssh, $data_options);
	}
}
sub on_bouton_sauvegarder_monter_new_clicked {
	debug("Bouton sauvegarder & monter");
	&on_bouton_monter_clicked(1);
}
sub on_bouton_monter_new_clicked {
	debug("Bouton monter");
	&on_bouton_monter_clicked(0);
}
# Montage d'une connexion enregistrée
sub on_bouton_monter_sav_clicked {
	debug("Montage d'une connexion enregistrée");
	my $nom_connexion_enregistree = $gladexml->get_widget('connexion_sav_combo')->entry->get_text();
	if ($nom_connexion_enregistree ne $params->{comboSetText} && $nom_connexion_enregistree ne "") {
		# Exploration du fichiers de sauvegarde
		mount_sshfs($configData->{"$nom_connexion_enregistree"}->{user}, 
					$configData->{"$nom_connexion_enregistree"}->{host},
					$configData->{"$nom_connexion_enregistree"}->{dir},
					$configData->{"$nom_connexion_enregistree"}->{mountpoint},
					$configData->{"$nom_connexion_enregistree"}->{port},
					$configData->{"$nom_connexion_enregistree"}->{keyssh}, 
					$configData->{"$nom_connexion_enregistree"}->{options});
	}
}
# Démontage
sub on_bouton_demonter_clicked {
	debug("Démontage un réperoire sshfs");
	my $a_deconnecter = $gladexml->get_widget('deconnexion_combo')->entry->get_text();
	if ($a_deconnecter ne $params->{comboSetText} && $a_deconnecter ne "") {
		umount_sshfs($a_deconnecter);
	}
}
# Effacer une connexion enregistrée
sub on_button_effacer_connexion_enregistree_clicked {
	debug("Effacer connexion enregistrée");
	&effacer_connexio_enregistree($gladexml->get_widget('connexion_sav_combo')->entry->get_text());
	&actualiser_connexion_sav_combo;
}
sub on_bouton_actualiser_deconnexion_clicked {
	&actualiser_deconnexion_combo;
}
sub on_bouton_ouvrir_clicked {
	debug("Ouverture d'un répertoire");
	my $a_ouvrir = $gladexml->get_widget('deconnexion_combo')->entry->get_text();
	if ($a_ouvrir ne $params->{comboSetText} && $a_ouvrir ne "") {
		system("$params->{opt_file_manager} $a_ouvrir");
	}
}
sub on_param_save_clicked{
	debug("Sauvegarde de la configuration");
	my $default_keyssh;
	if ($gladexml->get_widget('default_keyssh')->get_filenames()) {
		$default_keyssh = $gladexml->get_widget('default_keyssh')->get_filenames();
	} else {
		$default_keyssh = '';
	}
	$configParam->{"default"} = { 
		default_user         => $gladexml->get_widget('default_user')->get_text(), 
		default_dir          => $gladexml->get_widget('default_dir')->get_text(),
		default_mountpoint   => $gladexml->get_widget('default_mountpoint')->get_text(),
		default_port         => $gladexml->get_widget('default_port')->get_value(),
		default_keyssh       => $default_keyssh,
		default_options      => $gladexml->get_widget('default_options')->get_text()
	}; 
	$configParam->{"opt"} = { 
		opt_open_folder     => $gladexml->get_widget('opt_open_folder')->get_active(), 
		opt_file_manager    => $gladexml->get_widget('opt_file_manager')->get_text(),
		opt_path_sshfs      => $gladexml->get_widget('opt_path_sshfs')->get_text(),
		opt_path_fusermount => $gladexml->get_widget('opt_path_fusermount')->get_text()
	}; 
	$configParam->write($params->{configParam});
	$gladexml->get_widget('Param_Dialog')->hide;
	&param_ini
}
sub on_bouton_reprendre_clicked {
	my $nom_connexion_enregistree = $gladexml->get_widget('connexion_sav_combo')->entry->get_text();
	if ($nom_connexion_enregistree ne $params->{comboSetText} && $nom_connexion_enregistree ne "") {
		$gladexml->get_widget('user')->set_text($configData->{"$nom_connexion_enregistree"}->{user});
		$gladexml->get_widget('host')->set_text($configData->{"$nom_connexion_enregistree"}->{host});
		$gladexml->get_widget('dir')->set_text($configData->{"$nom_connexion_enregistree"}->{dir});
		$gladexml->get_widget('mountpoint')->set_text($configData->{"$nom_connexion_enregistree"}->{mountpoint});
		$gladexml->get_widget('port')->set_value($configData->{"$nom_connexion_enregistree"}->{port});
		debug($configData->{"$nom_connexion_enregistree"}->{port});
		if ($configData->{"$nom_connexion_enregistree"}->{keyssh}) {
			$gladexml->get_widget('keyssh')->set_filename($configData->{"$nom_connexion_enregistree"}->{keyssh}); 
		}
		$gladexml->get_widget('options')->set_text($configData->{"$nom_connexion_enregistree"}->{options});
	}
}
############
# Fonction / Action du progdefault valuesramme
############

# Fonction debug pour la prog 
sub debug {
	my $msg_debug=shift;
	if ($params->{debug} == 1) {
		print "Debug : $msg_debug\n";
	}
}
sub param_ini{
	debug("Initialisation des paramètres");
	if (-e $params->{configParam}) {
		debug("Lecture du fichier " . $params->{configParam});
		if ($configParam->{default}->{default_user}) {
			$params->{default_user} = $configParam->{default}->{default_user};
		}
		if ($configParam->{default}->{default_dir}) {
			$params->{default_dir} = $configParam->{default}->{default_dir};
		}
		if ($configParam->{default}->{default_mountpoint}) {
			$params->{default_mountpoint} = $configParam->{default}->{default_mountpoint};
		}
		if ($configParam->{default}->{default_port}) {
			$params->{default_port} = $configParam->{default}->{default_port};
		}
		if ($configParam->{default}->{default_options}) {
			$params->{default_options} = $configParam->{default}->{default_options};
		}
		if ($configParam->{default}->{default_keyssh}) {
			$params->{default_keyssh} = $configParam->{default}->{default_keyssh};
		}
		$params->{opt_open_folder} = $configParam->{opt}->{opt_open_folder};
		if ($configParam->{opt}->{opt_file_manager}) {
			$params->{opt_file_manager} = $configParam->{opt}->{opt_file_manager};
		}
		if ($configParam->{opt}->{opt_path_sshfs}) {
			$params->{opt_path_sshfs} = $configParam->{opt}->{opt_path_sshfs};
		}
		if ($configParam->{opt}->{opt_path_fusermount}) {
			$params->{opt_path_fusermount} = $configParam->{opt}->{opt_path_fusermount};
		}
	} 
	$gladexml->get_widget('user')->set_text($params->{default_user});
	$gladexml->get_widget('dir')->set_text($params->{default_dir});
	#$gladexml->get_widget('mountpoint')->set_text($params->{default_mountpoint});
	$gladexml->get_widget('port')->set_value($params->{default_port});
	if ($params->{default_keyssh}) {
		$gladexml->get_widget('keyssh')->set_filename($params->{default_keyssh}); 
	}
	$gladexml->get_widget('options')->set_text($params->{default_options});
}
# Actualise la combobox de sauvegarde
sub actualiser_connexion_sav_combo{
	debug("Actualisation de la combo de sauvegarde");
	my (@liste_sav);
	my $nb_item = 0;
	# Si le fichier de sauvegarde existe
	if (-e $params->{configData})
	{
		foreach my $section(sort keys %$configData) {
			@liste_sav = (@liste_sav, $section);
			$nb_item = ($nb_item + 1);
		}
	}
	if ($nb_item eq 0) {
		$gladexml->get_widget("connexion_sav_combo")->set_popdown_strings("");
		$gladexml->get_widget("connexion_sav_combo")->entry->set_text("");
	} else {
		$gladexml->get_widget("connexion_sav_combo")->set_popdown_strings(@liste_sav);
		$gladexml->get_widget("connexion_sav_combo")->entry->set_text($params->{comboSetText});    
	}
}
# Actualise la deconnexion combo
sub actualiser_deconnexion_combo {
	debug("Actualisation de la combo de déconnexion");
	my @liste_deco;
	my $nb_item = 0;
	open (FICMTAB, "/etc/mtab");		
	while (<FICMTAB>) {
		# Sélection des mount monté par l'utilisateur courant et étant du sshfs
		if (index($_, "sshfs") && index($_, "$ENV{USER}") ne -1 && index($_, ".gvfs") eq -1) {
			my @var_add_deco = split (" ", $_);
			@liste_deco = (@liste_deco, $var_add_deco[1]);
			$nb_item = ($nb_item + 1);
		}
	}
	close(FICMTAB);
	# Pour palier au BUG de l'actualisation quand il ne reste plus qu'une valeur on désactive le combo si elle est vide
	if ($nb_item eq 0) {
		$gladexml->get_widget("deconnexion_combo")->set_popdown_strings("");
		$gladexml->get_widget("deconnexion_combo")->entry->set_text("");
	}
	else {
		$gladexml->get_widget("deconnexion_combo")->set_popdown_strings(@liste_deco);
		$gladexml->get_widget("deconnexion_combo")->entry->set_text($params->{comboSetText});    
	}
}
# Complétation automatique du champ mountpoint suivant user & host
sub on_host_or_user_changed {
	my $data_user = $gladexml->get_widget('user')->get_text();
	my $data_host = $gladexml->get_widget('host')->get_text();
	if ($data_user ne "" & $data_host ne "" & $params->{default_mountpoint} ne "") {
		my $mountpoint_complet = "$params->{default_mountpoint}/$data_user\@$data_host";
		$gladexml->get_widget('mountpoint')->set_text($mountpoint_complet);
	} else {
		$gladexml->get_widget('mountpoint')->set_text("");
	}
}
# Enregistrement de la connexion - suite popoup d'enregistrement
sub Enregistrer_connexion {
	debug("Enregistrement de la connexion...");
	my $data_nom_sav = $gladexml->get_widget('Sav_Nom')->get_text();
	my $data_keyssh;
	if ($gladexml->get_widget('keyssh')->get_filenames()) {
		$data_keyssh = $gladexml->get_widget('keyssh')->get_filenames();
	} else {
		$data_keyssh = '';
	}
	# Vérification de la non existance de la connexion
	if ($configData->{$data_nom_sav}) {
		Erreur_Dialog(gettext("Can not record : the registration name already exists!"), "");
		&fermeture_nom_sav;
		my $Nom_Sav_Dialog = $gladexml->get_widget('Nom_Sav_Dialog')->run;
	} else {
		# Enregistrement de la connexion
		$configData->{"$data_nom_sav"} = { 
			user => $gladexml->get_widget('user')->get_text(), 
			host => $gladexml->get_widget('host')->get_text(),
			dir => $gladexml->get_widget('dir')->get_text(),
			mountpoint => $gladexml->get_widget('mountpoint')->get_text(),
			port => $gladexml->get_widget('port')->get_value(),
			options => $gladexml->get_widget('options')->get_text(),
			keyssh => $data_keyssh
		}; 
		$configData->write($params->{configData});
		&fermeture_nom_sav;
		&actualiser_connexion_sav_combo;
	}
}
sub effacer_connexio_enregistree{
	my ($nom_connexion_enregistree) = @_;
	debug("Effacement de $nom_connexion_enregistree");
	if ($nom_connexion_enregistree ne $params->{comboSetText}) {
		delete $configData->{$nom_connexion_enregistree};
		$configData->write($params->{configData});
	}
}
# Mount 
sub mount_sshfs {
	debug("Montage !");
	my ($data_user, $data_host, $data_dir, $data_mountpoint, $data_port, $data_keyssh, $data_options) = @_;
	# Vérification des datas
	if ($data_user eq "") {
		$data_user=$ENV{ USER};
	}
	if ($data_mountpoint eq "") {
		$data_mountpoint="$ENV{HOME}/$data_user\@$data_host";
	}
	if ($data_port eq "") {
		$data_port="22";
	}
	# Si le répertoire n'existe pas on le crée
	if (! -d $data_mountpoint) {
		mkdir $data_mountpoint;
	}
	# Préparation de la commande
	my $cmd="$params->{opt_path_sshfs} $data_user\@$data_host:$data_dir $data_mountpoint -p $data_port";
	if ($data_keyssh) {
		$cmd="$cmd -o IdentityFile=$data_keyssh";
	}
	if ($data_options) {
		$cmd="$cmd $data_options";
	}
	$cmd="$cmd 2>$params->{tmpFile}";
	# Exécution de la commande :
	debug($cmd);
	system($cmd);
	if ($? != 0) {
		open (TMPSORTIE, $params->{tmpFile});
		undef $/;
		my $retour_cmd = <TMPSORTIE>;
		close(TMPSORTIE);
		Erreur_Dialog(gettext("An error has occurred"), "$retour_cmd");
	} else {
		&nettoyer_fomulaire;
		&actualiser_deconnexion_combo;
		# Ouverture gestionnaire fichiers
		if ($params->{opt_open_folder}) {
			system("$params->{opt_file_manager} $data_mountpoint");
		}
	}
	unlink $params->{tmpFile};
}
# Démontage
sub umount_sshfs {
	debug("Démontage !");
	my ($a_demonter) = @_;
	system("$params->{opt_path_fusermount} -u $a_demonter 2> $params->{tmpFile}");
	if ($? != 0) {
		open (TMPSORTIE, "$params->{tmpFile}");
		undef $/;
		my $retour_cmd = <TMPSORTIE>;
		close(TMPSORTIE);
		Erreur_Dialog(gettext("An error has occurred"), "$retour_cmd");
	} else {
		&actualiser_deconnexion_combo;
		rmdir $a_demonter;
	}
	unlink "$params->{tmpFile}";
}
# Vide le formulaire graphique
sub nettoyer_fomulaire {
	&param_ini
}
sub touch {
	my $file = shift;
	if (! -e $file) {
		open (TOUCH, ">>$file");
		close (TOUCH); 
	} 
}
# Affiche les boite de dialog d'erreur
sub Erreur_Dialog{
	debug("Affichage de la boite de dialog d'erreur");
	my $msg_erreur = shift;
	my $detail_erreur = shift;
	$gladexml->get_widget('Erreur_Label')->set_text($msg_erreur);
	if ($detail_erreur eq "")
	{
		$gladexml->get_widget('erreur_text')->set_text("No details");
	} else {
		$gladexml->get_widget('erreur_text')->set_text($detail_erreur);
	}
	my $Erreur_Dialog = $gladexml->get_widget('Erreur_Dialog')->run;
}
1 ; 

package X ;

use strict;
use Locale::gettext;

require Exporter ;
use vars qw(@EXPORT_OK) ;
@EXPORT_OK = qw ($gladexml) ;
use vars qw($gladexml) ;

use Gtk2 '-init' ;
use Gtk2::GladeXML ;

# On crée l'arbre xml complet. Attention, toutes les fenêtres déclarées
# visibles dans le menu Propriétés->commun->visible, seront affichées
# quand on lancera "Gtk2->main".
$gladexml = Gtk2::GladeXML->new('xsshfs.glade' ) ;
# On initialise les variables du module rappels.
rappels::init () ;
# On connecte les fonctions de rappels de l'arbre xml
# à leurs définitions qui sont contenues dans le module rappels.
$gladexml->signal_autoconnect_from_package('rappels' ) ;

1 ;
