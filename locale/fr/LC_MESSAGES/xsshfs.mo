��    '      T  5   �      `     a     w     �     �     �      �     �  6   �     1     Q     W     d     j     �     �     �  
   �     �     �     �       
              ,     8  
   M     X     i     w     ~     �     �     �     �     �     �     �     �  �  �     �     �     �     �     �          #  @   =     ~     �     �     �     �     �     �     	     !	     1	     G	     O	     ^	     u	     �	     �	     �	     �	     �	     �	     �	     
  $   
     2
     >
     P
     W
  !   g
     �
     �
        %               $      	                           !      &         "   #                                   '          
                                                          <b>Default values</b> <b>Mounted</b> <b>New Connection</b> <b>Options</b> <b>Saved connection</b> Address of the remote ssh server An error has occurred Can not record : the registration name already exists! Enter a name for the connection Error File manager Hel_p Local path to mount sshfs Location of the remote folder Mount point Name of the remote user No details Open the folder after connect Options Other options Path fusermount Path sshfs Port number Private key Refer to "man sshfs" Remote dir Save and connect Select a file Server Settings The server address is required User View details Xsshfs Xsshfs - Error Xsshfs - Save connection Your SSH key _File Project-Id-Version: xsshfs 0.5
Report-Msgid-Bugs-To: david . mercereau [arobase] gmail . com 
POT-Creation-Date: 2012-07-06 10:46+0200
PO-Revision-Date: 2012-07-06 10:46+0200
Last-Translator: David Mercereau <david . mercereau [arobase] gmail . com>
Language-Team: FR <david . mercereau [arobase] gmail . com>
Language: fr
MIME-Version: 1.1
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 <b>Valeurs par défaut</b> <b>Monter</b> <b>Nouvelle connexion</b> <b>Options</b> <b>Connexion sauvegardée</b> Adresse du serveur distant Une erreur s'est produite Impossible d'enregistrer: le nom d'enregistrement existe déjà! Entrer le nom de la connexion Erreur Gestionnaire de fichiers _Aide Point de montage sshfs local Répertoire distant Point de montage Nom de l'utilisateur distant Pas de détails Ouvrir le répertoire Options Autres options Chemin vers fusermount Chemin vers sshfs Numéro de port Clef privée c.f. man xsshfs Répertoire distant Sauvegarder & connecter Sélectionner un fichier Serveur Paramètres L'adresse du serveur est obligatoire Utilisateur Voir les détails Xsshfs Xsshfs - Erreur Xsshfs - Sauvegarder la connexion Votre clef SSH _Fichier 