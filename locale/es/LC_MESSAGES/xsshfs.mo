��    '      T  5   �      `     a     w     �     �     �      �     �  6   �     1     Q     W     d     j     �     �     �  
   �     �     �     �       
              ,     8  
   M     X     i     w     ~     �     �     �     �     �     �     �     �  �  �     �     �     �     �     �     �       4   ,  #   a     �     �     �     �     �     �     �     
	     	     6	     ?	     N	     `	     m	     t	     �	     �	     �	     �	     �	     �	  &   �	     	
     
     
     %
     4
     O
     \
        %               $      	                           !      &         "   #                                   '          
                                                          <b>Default values</b> <b>Mounted</b> <b>New Connection</b> <b>Options</b> <b>Saved connection</b> Address of the remote ssh server An error has occurred Can not record : the registration name already exists! Enter a name for the connection Error File manager Hel_p Local path to mount sshfs Location of the remote folder Mount point Name of the remote user No details Open the folder after connect Options Other options Path fusermount Path sshfs Port number Private key Refer to "man sshfs" Remote dir Save and connect Select a file Server Settings The server address is required User View details Xsshfs Xsshfs - Error Xsshfs - Save connection Your SSH key _File Project-Id-Version: xsshfs 0.5
Report-Msgid-Bugs-To: david . mercereau [arobase] gmail . com 
POT-Creation-Date: 2012-07-06 10:46+0200
PO-Revision-Date: 2012-07-06 10:46+0200
Last-Translator: Fernando Canizo <conan [arobase] lugmen . org . ar> 
Language-Team: ES <conan [arobase] lugmen . org . ar>
Language: es
MIME-Version: 1.1
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 <b>Valores por omisión</b> <b>Montado</b> <b>Nueva conexión</b> <b>Opciones</b> <b>Conexión guardada</b> Dirección del servidor remoto Se produjo un error No se puede grabar: el nombre de registro ya existe! Ingrese un nombre para la conexión Error Gestor de archivos A_yuda Carpeta local para montar Ubicación de la carpeta remota Punto de montaje Nombre del usuario remoto Sin detalles Abrir la carpeta tras conectar Opciones Otras opciones Camino fusermount Camino sshfs Puerto Clave privada Ver "man sshfs" Carpeta remota Guardar y conectar Seleccione un archivo Servidor Configuración Se requiere la dirección del servidor Usuario Ver detalles Xsshfs Xsshfs - Error Xsshfs - Guardar conexión Su clave SSH _Archivo 